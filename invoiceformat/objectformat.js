const invoice_load = {
        client_name : "Alvin Januar",
        client_address : "796 Silver Harbour, TX 79273, US",
        client_email : "alvinjanuar@gmail.com",
        invoice_number : "0x1fb217f8af07b337ed3eee87da69063e5d8dc3cefde9ddbf2924b81959c4feeb",
        made_date : "01/06/2020",
        due_date : "30/06/2020",
        service : [
          {
            title : "Website Design",
            description : "Creating a recognizable design solution based on the company&apos;s existing visual identity",
            unit_price : 40,
            quantity : 30
          }
        ],
        vat : 30,
        added_notice : "A finance charge of 1.5% will be made on unpaid balances after 30 days.",
        currency : "NTD"
      };