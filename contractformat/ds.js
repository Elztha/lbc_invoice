		let header = "Services Agreement";
		let contract_hash = "#HS341234123412341234"
		let state = "Taiwan"
		let date = {
			day:'30' ,
			month:'December' ,
			year:'2019'
		}
		let seller = {
			fullname:'Ecloudture' ,
			address:'7F., No.111, Sec. 4, Sanhe Rd., Sanchong Dist., New Taipei City 24152, Taiwan'
		}
		let buyer = {
			fullname:'Tester' ,
			address:'3 Church Street, Samsung Hub, Level 8, Singapore 049483'
		}

		let services = [
		{
			description:'Cleaning Bathroom' ,
			quantity:'1' ,
			quantity_price:'300',
			price_currency:'USD'
		},
		{
			description:'Washing Dishes' ,
			quantity:'1' ,
			quantity_price:'300',
			price_currency:'USD'
		}
		]

		let total_fee = '$1,000.00'
		let tax_by_buyer = true

		let payment = {
			method:'cash',
			data:[
				{_class:'prepaid_payment',amount:'10.00'},
				{_class:'lease_incentive',amount:'100.00'},
				{_class:'closing_payment',amount:'10.00'}		
			]

		}

		let inspection ={
			rights : true,
			deadline : 10 ,
			options : {
				revision : 0,
				terminate : '50%'
			}
		}