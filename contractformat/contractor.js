// 0
		let header = "Services Agreement";
		let contract_hash = "#HS341234123412341234"
		let state = "Taiwan"
		let date = {
			day:'30' ,
			month:'December' ,
			year:'2019'
		}
		let seller = {
			fullname:'Alvin Januar' ,
			address:'PT ASIANAGRO AGUNGJAYA, JL SEMARANG BLOK A6 NO.1, JAKARTA UTARA, CA 14120'
		}
		let buyer = {
			fullname:'BAYER' ,
			address:'PT ASIANasdf BLOK A6 NO.1, JAKARTA UTARA, CA 14120'
		}

		// Biodata of Entities
		$(".header").html(header);
		$("#contract_hash").html(contract_hash);
		$("#state").html('State of '+ state);
		$("#date_day").html(date.day);
		$("#date_month").html(date.month);
		$("#date_year").html(date.year);
		$("#seller_fullname").html(seller.fullname);
		$("#seller_location").html(seller.address);
		$("#buyer_fullname").html(buyer.fullname);
		$("#buyer_location").html(buyer.address);
		$("#buyer_location").html(buyer.address);

// 1
		let services = [
		{
			description:'Cleaning Bathroom' ,
			quantity:'1' ,
			quantity_price:'300',
			price_currency:'USD'
		},
		{
			description:'Washing Dishes' ,
			quantity:'1' ,
			quantity_price:'300',
			price_currency:'USD'
		}
		]
		// Services Quantity details
		for(let i = 0; i < services.length ; i++){
		let desc= services[i].description;
        let quantity = services[i].quantity;
        let quantity_price = services[i].quantity_price;
        let quantity_currency = services[i].price_currency;
        let markup = "<tr><td>" + desc + "</td><td>" + quantity + "</td><td>" + quantity_price +" "+ quantity_currency+"</td></tr>";
        $("table tbody").append(markup);
		}

// 2nd
		let total_fee = '$1,000.00'
		let tax_by_buyer = true

		// Total Fee
		$("#total_fee").html(total_fee);

		// Tax paying entities
		if(tax_by_buyer){$("#tax_payer").html("Buyer");}
		else{$("#tax_payer").html("Seller");}

// 3rd
		let payment = {
			method:'cash',
			data:[
				{_class:'prepaid_payment',amount:'10.00'},
				{_class:'lease_incentive',amount:'100.00'},
				{_class:'closing_payment',amount:'10.00'}		
			]

		}

		// Payment Methods
		let method= payment.method;

		// Payment Details
		for(let i = 0; i < payment.data.length ; i++){
        let _class = payment.data[i]._class;
        let amount = payment.data[i].amount;
        let price_currency = payment.data[i].price_currency;
        let payment_data;
        switch(_class) {
        	case 'prepaid_payment':
 		        payment_data = "<li>$ " + amount +" previously paid by Buyer.</li>";
  				break;
    		case 'lease_incentive':
 		        payment_data = "<li>$ " + amount +" upon the execution of this Agreement.</li>";
    			break;
    		case 'closing_payment':
 		        payment_data = "<li>$ " + amount +" upon the completion of the services.</li>";
    			break;
    		default:
    			payment_data = "<li>$ " + amount +" as written on <b>17.Others</b></li>";
    			break;
		}
        $("#payment_data").append(payment_data);
		}

//4th
		let inspection ={
			rights : true,
			deadline : 10 ,
			options : {
				revision : 0,
				terminate : '50%'
			}

		}

		if(inspection.rights){
			$(".inspection_deadline").html(inspection.deadline);
			if(inspection.options.revision > 0){
			$("#termination").html("- request "+ inspection.options.revision + " revision of the product provided<br>");
			}
			if(inspection.options.terminate){
			$("#termination").append("- terminate the contract following payment for "+ inspection.options.terminate + " of the services ");
			}
		}