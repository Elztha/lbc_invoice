const Web3 = require('web3')
let web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))
var account = "0xf222DF1463F9CCFA580dd917f42DfDFFcb869d3f"
abi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "hash",
				"type": "string"
			}
		],
		"name": "add",
		"outputs": [
			{
				"name": "a",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "hash",
				"type": "string"
			}
		],
		"name": "verify",
		"outputs": [
			{
				"name": "dateAdded",
				"type": "uint256"
			},
			{
				"name": "blockAdded",
				"type": "uint256"
			},
			{
				"name": "hasher",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "dateAdded",
				"type": "uint256"
			},
			{
				"indexed": false,
				"name": "blockAdded",
				"type": "uint256"
			}
		],
		"name": "Added",
		"type": "event"
	}
]
contract = new web3.eth.Contract(abi);
contract.options.address = "0x9E9Da9cC335b1574c0EAe7C32F99C055Fee0Cc88";
// update this contract address with your contract address

function convertTimestamp(time) {
            var d = new Date(time * 1000), // Convert the passed timestamp to milliseconds
                yyyy = d.getFullYear(),
                mm = ('0' + (d.getMonth() + 1)).slice(-2),  // Months are zero based. Add leading 0.
                dd = ('0' + d.getDate()).slice(-2),         // Add leading 0.
                hh = d.getHours(),
                h = hh,
                min = ('0' + d.getMinutes()).slice(-2),     // Add leading 0.
                ampm = 'AM',
                time;if (hh > 12) {
                h = hh - 12;
                ampm = 'PM';
            } else if (hh === 12) {
                h = 12;
                ampm = 'PM';
            } else if (hh == 0) {
                h = 12;
            }// ie: 2014-03-24, 3:00 PM
            time1 = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
            return time1;
      }  

async function upload_validate(json_value) {
 var documents;
 await contract.methods.add(web3.utils.asciiToHex(json_value)).send({from: account, gas:3000000}).then((f) => {
 	documents = {
 	txhash	: f.transactionHash,
 	dateAdded : convertTimestamp(web3.eth.abi.decodeParameters([{type:'uint256', name: 'dateAdded'},{type:'uint256', name: 'blockAdded'}],f.events.Added.raw.data).dateAdded),
 	blockAdded : web3.eth.abi.decodeParameters([{type:'uint256', name: 'dateAdded'},{type:'uint256', name: 'blockAdded'}],f.events.Added.raw.data).blockAdded,
 	}
}).catch(console.log);
 return documents;
}

async function upload(json_value){
	let a = await upload_validate(json_value);
	return a;
}

async function verify_validate(json_value){
  var documents;
  await contract.methods.verify(web3.utils.asciiToHex(json_value)).call({from: account}).then((f) => {
  	if(f.hasher){
  		documents = {
    dateAdded : convertTimestamp(f.dateAdded),
 	blockAdded : f.blockAdded,
 	containing : web3.utils.toAscii(f.hasher)
		}
  	}
  	else{
  		documents = {
  			error :"Data has been compromised" 
  		};
  	}
}).catch(console.log)
  return documents;
}

async function verify(json_value){
	let a = await verify_validate(json_value);
	return a;
}


module.exports.upload = upload;
module.exports.verify = verify;

