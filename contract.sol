pragma solidity ^0.4.25;

contract DocumentRegistry {
    mapping (string => biz_con) documents;
    address contractOwner = msg.sender;
    
    struct biz_con{
        uint256 dateAdded;
        uint256 blockAdded;
        string containing;
    }
    
    event Added(
        uint256 dateAdded,
        uint256 blockAdded
    );

    function add(string hash) public returns(uint256 a){
        require (msg.sender == contractOwner);
        documents[hash] = biz_con(block.timestamp,block.number,hash);
        emit Added(block.timestamp,block.number);
        return block.timestamp;
    }

    function verify(string hash) constant public returns (uint256 dateAdded,uint256 blockAdded,string hasher) {
        return (documents[hash].dateAdded,documents[hash].blockAdded,documents[hash].containing);
    }
}
